import Vue from "vue";
import Vuex from "vuex";
import * as FirebaseRepository from "../repositories/firebaseRepository";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    currentMatch: {},
    currentMatchID: "",
    playerOneName: "",
    playerTwoName: "",
    isLoadingData: false,
    turnNumber: 0,
    playerMoves: [],
  },
  getters: {
    currentMatchID: (state) => state.currentMatchID,
    currentMatch: (state) => state.currentMatch,
    playerOneName: (state) => state.playerOneName,
    playerTwoName: (state) => state.playerTwoName,
    isLoadingData: (state) => state.isLoadingData,
  },
  mutations: {
    SET_CURRENT_MATCH(state, payload) {
      state.currentMatch = payload;
      state.playerOneName = payload.playerOneName;
      state.playerOneName = payload.playerTwoName;
      state.playerTwoName = payload.turnNumber;
      state.playerMoves = payload.playerMoves;
    },
    SET_CURRENT_MATCH_ID(state, payload) {
      state.currentMatchID = payload;
    },
    SET_IS_LOADING_DATA(state, payload) {
      state.isLoadingData = payload;
    },
    SET_PLAYER_NAMES(state, payload) {
      state.playerOneName = payload.playerOne;
      state.playerTwoName = payload.playerTwo;
    },
  },
  actions: {
    async saveMatch({ commit }, params) {
      if (!params.id) commit("SET_IS_LOADING_DATA", true);
      const result = await FirebaseRepository.saveMatch(params.id, params);
      if (result) {
        commit("SET_CURRENT_MATCH_ID", result.id);
      }
      commit("SET_CURRENT_MATCH", params);

      if (!params.id) commit("SET_IS_LOADING_DATA", false);
    },
    async getMatchById({ commit }, id) {
      const result = await FirebaseRepository.getMatchById(id);
      if (result) {
        commit("SET_CURRENT_MATCH", result);
      }
    },
    async savePlayerNames({ commit }, names) {
      commit("SET_PLAYER_NAMES", names);
    },
    async setCurrentMatchID({ commit }, id) {
      commit("SET_CURRENT_MATCH_ID", id);
    },
  },
  modules: {},
});
