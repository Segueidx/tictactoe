import { initializeApp } from "firebase/app";
import {
  getFirestore,
  getDoc,
  doc,
  setDoc,
  addDoc,
  collection,
} from "firebase/firestore/lite";

const firebaseConfig = {
  apiKey: "AIzaSyAtDXmIwYGA7spOEZy1cojqqiSvEL-h0BM",
  authDomain: "tictactoe-c5f20.firebaseapp.com",
  projectId: "tictactoe-c5f20",
  storageBucket: "tictactoe-c5f20.appspot.com",
  messagingSenderId: "938592286513",
  appId: "1:938592286513:web:b9c265e9f1b7a9802aa210",
  measurementId: "G-ZB14YF99LG",
};

const app = initializeApp(firebaseConfig);
const db = getFirestore(app);

async function getMatchById(id) {
  const lastMatch = doc(db, "matches", id);
  return (await getDoc(lastMatch)).data();
}

async function saveMatch(id, params) {
  if (id) await setDoc(doc(db, "matches", params.id), params);
  else return await addDoc(collection(db, "matches"), params);
}

async function updateLastMatchID(id) {
  await setDoc(doc(db, "matches", "globals"), { currentMatch: id });
}

export { getMatchById, saveMatch, updateLastMatchID };
